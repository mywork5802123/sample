package com.example.demo.restcontroller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Product;




@RestController
public class ProductServiceController {
	
	private static Map<String, Product> productRepo = new HashMap<>();
	
	static {
	      Product honey = new Product();
	      honey.setId("1");
	      honey.setName("Honey");
	      productRepo.put(honey.getId(), honey);
	      
	      Product almond = new Product();
	      almond.setId("2");
	      almond.setName("Almond");
	      productRepo.put(almond.getId(), almond);
	   }
	
	@RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
	   public void displayProduct(@PathVariable("id") String id) { 
		System.out.println("Request parameter-"+id);
	      
	   }
	
	@RequestMapping(value = "/products", method = RequestMethod.POST)
	   public ResponseEntity<Object> createProduct(@Valid @RequestBody Product product) {
		System.out.println("Product ID-"+product.getId());
		System.out.println("Product Name-"+product.getName());
	      //productRepo.put(product.getId(), product);
	      return new ResponseEntity<>("Product is created successfully", HttpStatus.CREATED);
	   }
	
	
}
